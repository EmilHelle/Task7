﻿using System;
using System.Collections.Generic;

namespace Task7
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");


            foreach (int number in FibonacciSequence())
            {
                Console.WriteLine(number);
            }
            Console.WriteLine("Sum of even numbers: " + SumOfEvenNumbers(FibonacciSequence()));

 
        }

        public static int SumOfEvenNumbers(List<int> numbers)
        {
            int sum = 0;
            foreach (int number in numbers)
            {
                if(number % 2 == 0)
                {
                    sum += number;
                }
            }
            return sum;
        }

        public static int Fibonacci(int number)
        {
            int a = 1;
            int b = 2;

            for (int i = 0; i < number; i++)
            {
                int temporary = a;
                a = b;
                b = temporary + b;
            }
            return a;
        }

        public static List<int> FibonacciSequence()
        {
            List<int> numbers = new List<int>();
            for (int i = 0; Fibonacci(i) <4000000; i++)
            {
                numbers.Add(Fibonacci(i));
            }
            return numbers;
        }
    }
}
